#!/bin/bash

add-apt-repository -y ppa:x2go/stable

apt-get update
apt-get -y upgrade

localectl set-locale LANG="de_DE.UTF-8"
localectl set-locale LANGUAGE="de_DE.UTF-8"

apt-get install -y kubuntu-desktop
#gnome-session unity
#ubuntu-desktop  gconf-service
apt-get install -y virtualbox-guest-dkms x2goserver x2goserver-xsession

grep -q -F 'allowed_users=anybody' /etc/X11/Xwrapper.config || echo 'allowed_users=anybody' >> /etc/X11/Xwrapper.config

update-rc.d x2goserver defaults
/etc/init.d/x2goserver start

source /vagrant/vagrant/provision-custom.sh
