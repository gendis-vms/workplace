class Gendisvm
  def Gendisvm.config(config, settings)

    config.vm.network "forwarded_port", guest: 22, host: settings["ssh"]

  end
end
